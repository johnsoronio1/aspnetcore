﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Logging;
using RabbitMQReceiver.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace RabbitMQReceiver.Notification
{
    public class NotificationClient
    {
        private readonly HubConnection _connection;

        public NotificationClient() : this("http://host.docker.internal:5002/hub") { }

        public NotificationClient(string url) {
            _connection = new HubConnectionBuilder()
                .WithUrl(url, trans => {
                    trans.SkipNegotiation = true;
                    trans.Transports = Microsoft.AspNetCore.Http.Connections.HttpTransportType.WebSockets;
                })
                .WithAutomaticReconnect()
                .ConfigureLogging(logging => {
                    logging.SetMinimumLevel(LogLevel.Information);
                    logging.AddConsole();
                })
                .Build();
        }

        public void StartConnection() {
            _connection.StartAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine("There was an error opening the connection:{0}",
                    task.Exception.GetBaseException());
                }
                else
                {
                    Console.WriteLine("Connected");
                }
            }).Wait();
        }

        public void StopConnection()
        {
            _connection.StopAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {
                    Console.WriteLine("There was an error opening the disconnection:{0}",
                    task.Exception.GetBaseException());
                }
                else {
                    Console.WriteLine("Disconnected");
                }
            }).Wait();
        }

        public void Push(User entry) 
        {
            _connection.InvokeAsync("PushEntry", entry);
        }
    }
}
