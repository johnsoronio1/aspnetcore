﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQReceiver.Models;
using RabbitMQReceiver.Notification;
using RabbitMQReceiver.Service;
using System;
using System.IO;

namespace RabbitMQReceiver
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfigurationRoot configuration = builder.Build();
          
            using (var manager = new RabbitMQManager(configuration))
            {
                manager.MessageReceived += (sender, msg) => Console.WriteLine(msg);
                manager.Connect();
            }
        }    
    }
}
