﻿using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQReceiver.Models;
using RabbitMQReceiver.Notification;
using System;
using System.Text;
using System.Threading;

namespace RabbitMQReceiver.Service
{
    public class RabbitMQManager : IDisposable
    {
        private readonly IConfigurationRoot _configuration;

        public RabbitMQManager(IConfigurationRoot configuration)
        {
            _configuration = configuration;
        }

        public event EventHandler<string> MessageReceived;

        public void Connect()
        {
            Console.WriteLine("----------------------------------------------------------------------------------------------------------");
            Console.WriteLine("-   Creating Connection with RabbitMQ:                                                                   -");
            Console.WriteLine("----------------------------------------------------------------------------------------------------------");

            var factory = new ConnectionFactory()
            {
                HostName = _configuration.GetValue<string>("RabbitMQConfig:HostName"),
                Port = _configuration.GetValue<int>("RabbitMQConfig:Port"),
                UserName = _configuration.GetValue<string>("RabbitMQConfig:UserName"),
                Password = _configuration.GetValue<string>("RabbitMQConfig:Password"),
                VirtualHost = _configuration.GetValue<string>("RabbitMQConfig:VirtualHost"),
                RequestedHeartbeat = 30
            };

            while (true)
            {
                try
                {
                    NotificationClient client = new NotificationClient();

                    string QueueName = "userInsertQueue";
                    using (var connection = factory.CreateConnection())
                    using (var channel = connection.CreateModel())
                    {
                        // We will also have to declare the queue here,
                        // because this application might start first so we will make sure that the queue exists before receiving messages.
                        channel.QueueDeclare(queue: QueueName,      // The name of the queue
                                             durable: false,         // true if we are declaring a durable queue(the queue will survive a server restart)
                                             exclusive: false,      // true if we are declaring an exclusive queue (restricted to this connection)
                                             autoDelete: false,     // true if we are declaring an auto delete queue (server will delete it when no longer in use)
                                             arguments: null);      // other properties (construction arguments) for the queue

                        channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

                        var consumer = new EventingBasicConsumer(channel);

                        consumer.Received += (model, ea) =>
                        {
                            var body = ea.Body;
                            var message = Encoding.UTF8.GetString(body);

                            client.StartConnection();
                            client.Push(JsonConvert.DeserializeObject<User>(message));
                            client.StopConnection();

                            Console.WriteLine("-----------------------------------------------------------------------------------------------------------");
                            Console.WriteLine("-   Receiving Data submitted from the WebApp                                                              -");
                            Console.WriteLine("-----------------------------------------------------------------------------------------------------------");

                            MessageReceived?.Invoke(this, $"Received: {message} {Thread.CurrentThread.ManagedThreadId}");

                            channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                        };

                        channel.BasicConsume(queue: QueueName,      // the name of the queue
                                             autoAck: false,        // true if the server should consider messages acknowledged once delivered; 
                                                                    // false if the server should expect explicit acknowledgements
                                             consumer: consumer);   // an interface to the consumer object

                        Console.WriteLine("Waiting for messages...");

                        Console.ReadLine();
                        break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Reconnect failed!");
                    Thread.Sleep(3000);
                }
            }
        }

        public void Dispose()
        {

        }
    }
}
