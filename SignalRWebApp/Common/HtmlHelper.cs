﻿using Microsoft.AspNetCore.Html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRWebApp.Common
{
    public enum HtmlDocType
    {
        Xhtml10Strict,
        Xhtml10Transitional,
        Xhtml10Frameset,
        Xhtml11,
        Html401Strict,
        Html401Transitional,
        Html401Frameset,
        Html5
    }

    public static class Helper
    {
        /// <summary>
        /// Doctype of your HTML document
        /// </summary>
        public static HtmlString DocType(HtmlDocType docType)
        {
            HtmlString str = null;
            switch (docType)
            {
                case HtmlDocType.Xhtml10Strict:
                    str = new HtmlString("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
                    break;
                case HtmlDocType.Xhtml10Transitional:
                    str = new HtmlString("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
                    break;
                case HtmlDocType.Xhtml10Frameset:
                    str = new HtmlString("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Frameset//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd\">");
                    break;
                case HtmlDocType.Xhtml11:
                    str = new HtmlString("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.1//EN\" \"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd\">");
                    break;
                case HtmlDocType.Html401Strict:
                    str = new HtmlString("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">");
                    break;
                case HtmlDocType.Html401Transitional:
                    str = new HtmlString("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\"> ");
                    break;
                case HtmlDocType.Html401Frameset:
                    str = new HtmlString("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Frameset//EN\"  \"http://www.w3.org/TR/html4/frameset.dtd\"> ");
                    break;
                case HtmlDocType.Html5:
                    str = new HtmlString("<!DOCTYPE html>");
                    break;
                default:
                    break;
            }
            return str;
        }

        /// <summary>
        /// Links all given external stylesheet files
        /// </summary>
        /// <param name="path">Paths of the stylesheet files</param>
        public static HtmlString Stylesheet(params string[] paths)
        {
            string temp = "";
            foreach (string path in paths)
            {
                temp += "<link href=\"" + path + "\" rel=\"stylesheet\" type=\"text/css\" />\n";
            }
            HtmlString str = new HtmlString(temp);
            return str;
        }

        /// <summary>
        /// Links all given external javascript files
        /// </summary>
        /// <param name="path">Paths of the script files</param>
        public static HtmlString Script(params string[] paths)
        {
            string temp = "";
            foreach (string path in paths)
            {
                temp += "<script src=\"" + path + "\" type=\"text/javascript\"></script>\n";
            }
            HtmlString str = new HtmlString(temp);
            return str;
        }

        /// <summary>
        /// Hyperlink to an URL
        /// </summary>
        /// <param name="path">URL</param>
        /// <param name="text">Text to be displayed</param>
        public static HtmlString Hyperlink(string path, string text)
        {
            HtmlString str = new HtmlString("<a href=\"" + path + "\">" + text + "</a>");
            return str;
        }

        /// <summary>
        /// Image
        /// </summary>
        /// <param name="path">Location of the image file</param>
        /// <param name="alt">ALT text</param>
        public static HtmlString Image(string path, string alt)
        {
            HtmlString str = new HtmlString("<img alt=\"" + alt + "\" src=\"" + path + "\"></img>");
            return str;
        }

        /// <summary>
        /// Image
        /// </summary>
        /// <param name="path">Location of the image file</param>
        /// <param name="alt">ALT text</param>
        /// <param name="width">Width of the image</param>
        /// <param name="height">Height of the image</param>
        public static HtmlString Image(string path, string alt, int width, int height)
        {
            HtmlString str = new HtmlString("<img alt=\"" + alt + "\" src=\"" + path + "\" width=\"" + width + "\" height=\"" + height + "\"></img>");
            return str;
        }

        /// <summary>
        /// Label control
        /// </summary>
        /// <param name="text">Text to be displayed</param>
        public static HtmlString Label(string text)
        {
            HtmlString str = new HtmlString("<label>" + text + "</label>");
            return str;
        }

        /// <summary>
        /// Label to be displayed
        /// </summary>
        /// <param name="text">Text to be displayed</param>
        /// <param name="forCtrl">Id of control to be associated with the label</param>
        public static HtmlString Label(string text, string forCtrl)
        {
            HtmlString str = new HtmlString("<label for=\"" + forCtrl + "\">" + text + "</label>");
            return str;
        }

        /// <summary>
        /// Button control
        /// </summary>
        /// <param name="text">Text to be displayed</param>
        /// <param name="id">Id of the button</param>
        public static HtmlString Button(string text, string id)
        {
            HtmlString str = new HtmlString("<button id=\"" + id + "\">" + text + "</button>");
            return str;
        }

        /// <summary>
        /// Radio button
        /// </summary>
        /// <param name="text">Text to be displayed</param>
        /// <param name="value">Value</param>
        /// <param name="group">Group name</param>
        /// <param name="id">Id of the radio button</param>
        public static HtmlString Radio(string text, string value, string group, string id)
        {
            HtmlString str = new HtmlString("<input type=\"radio\" val=\"" + value + "\" name=\"" + group + "\" id=\"" + id + "\"></radio>" + Label(text, id));
            return str;
        }

        /// <summary>
        /// Checkbox
        /// </summary>
        /// <param name="text">Text to be displayed</param>
        /// <param name="value">Value</param>
        /// <param name="id">Id of the check box</param>
        public static HtmlString Checkbox(string text, string value, string id)
        {
            HtmlString str = new HtmlString("<input type=\"checkbox\" val=\"" + value + "\" id=\"" + id + "\"></radio>" + Label(text, id));
            return str;
        }

        /// <summary>
        /// Textbox
        /// </summary>
        /// <param name="id">Id of the textbox</param>
        public static HtmlString Textbox(string id)
        {
            HtmlString str = new HtmlString("<input type=\"text\" id=\"" + id + "\"></input>");
            return str;
        }

        /// <summary>
        /// Password
        /// </summary>
        /// <param name="id">Id of the password box</param>
        public static HtmlString Password(string id)
        {
            HtmlString str = new HtmlString("<input type=\"password\" id=\"" + id + "\"></input>");
            return str;
        }

        /// <summary>
        /// File Input
        /// </summary>
        /// <param name="id">Id of the file upload control</param>
        public static HtmlString FileUpload(string id)
        {
            HtmlString str = new HtmlString("<input type=\"file\" id=\"" + id + "\"></input>");
            return str;
        }
    }
}
