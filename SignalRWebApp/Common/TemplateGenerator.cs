﻿using SignalRWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SignalRWebApp.Common
{
    public static class TemplateGenerator
    {
        public static string GetHTMLString(IList<User> users)
        {
            var index = 1;
            var sb = new StringBuilder();

            sb.Append(@"
                        <html>
                            <head>
                            </head>
                            <body>
                                <div class='header'><h1>This is the generated PDF report!!!</h1></div>
                                <table align='center'>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>LastName</th>
                                        <th>Age</th>
                                    </tr>");

            foreach (var user in users)
            {
                sb.AppendFormat(@"<tr>
                                    <td>{0}</td>
                                    <td>{1}</td>
                                    <td>{2}</td>
                                    <td>{3}</td>
                                  </tr>", index, user.FirstName, user.LastName, user.EmailAddress);
            }

            sb.Append(@"
                                </table>
                            </body>
                        </html>");

            return sb.ToString();
        }
    }
}
