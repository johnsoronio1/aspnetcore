﻿using SignalRWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRWebApp.SignalR
{
    public interface INotification
    {
        Task PushEntry(User user);
        Task PushMessage(string user, string message);
        Task Online(int userId);
        Task Offline(int userId);
    }
}
