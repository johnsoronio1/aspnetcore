﻿using Microsoft.AspNetCore.SignalR;
using SignalRWebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRWebApp.SignalR
{
    public class NotificationHub : Hub<INotification>
    {
        private readonly static ConnectionMapping<string> Connections = new ConnectionMapping<string>();
        public NotificationHub()
        {

        }

        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exp)
        {
            return base.OnDisconnectedAsync(exp);
        }

        public void Send(string user = "", string message = "")
        {
            Clients.All.PushMessage(user, message);
        }

        public void PushEntry(User entry)
        {
            Clients.All.PushEntry(entry);
        }
    }
}
