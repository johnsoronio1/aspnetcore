﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using SignalRWebApp.Common;
using SignalRWebApp.Models;
using SignalRWebApp.SignalR;
using Wkhtmltopdf.NetCore;

namespace SignalRWebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly IHubContext<NotificationHub> _hubContext;
        private readonly ILogger<HomeController> _logger;
        private readonly IGeneratePdf _generatePdf;
        private readonly IConverter _converter;

        public HomeController(ILogger<HomeController> logger, IHubContext<NotificationHub> hubContext, IGeneratePdf generatePdf, IConverter converter)
        {
            _logger = logger;
            _hubContext = hubContext;
            _generatePdf = generatePdf;
            _converter = converter;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> DemoViewAsPDF(IList<User> users)
        {
            var viewHtmlString = await this.RenderViewAsync("DemoViewAsPDF", users);

            var pdf = _generatePdf.GetPDF(viewHtmlString);
            var pdfStream = new System.IO.MemoryStream();
            pdfStream.Write(pdf, 0, pdf.Length);
            pdfStream.Position = 0;

            return File(pdfStream, "application/pdf", "EmployeeReport.pdf");
        }

        [HttpPost]
        [AllowAnonymous]
        public IActionResult DinkToPdf(IList<User> users)
        {
            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = "PDF Report"
            };

            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = TemplateGenerator.GetHTMLString(users),
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/css", "site.css") },
                HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "Page [page] of [toPage]", Line = true },
                FooterSettings = { FontName = "Arial", FontSize = 9, Line = true, Center = "Report Footer" }
            };

            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };

            _converter.Convert(pdf);

            var file = _converter.Convert(pdf);
            return File(file, "application/pdf", "EmployeeReport.pdf");
        }
    }
}
