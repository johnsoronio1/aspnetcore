﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQSender.Models;

namespace RabbitMQSender.Controllers
{
    [AllowAnonymous]
    public class UserController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<UserController> _logger;

        public UserController(IConfiguration configuration, ILogger<UserController> logger) 
        {
            _configuration = configuration;
            _logger = logger;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SendUserQueue()
        {
            User viewmodel = new User();

            return View(viewmodel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SendUserQueue(User viewmodel)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string senderUniqueId = "userInsertQueue";
                    var factory = new ConnectionFactory()
                    {
                        HostName = _configuration.GetValue<string>("RabbitMQConfig:HostName"),
                        Port = _configuration.GetValue<int>("RabbitMQConfig:Port"),
                        UserName = _configuration.GetValue<string>("RabbitMQConfig:UserName"),
                        Password = _configuration.GetValue<string>("RabbitMQConfig:Password"),
                        VirtualHost = _configuration.GetValue<string>("RabbitMQConfig:VirtualHost"),
                    };

                    _logger.LogInformation("Creating RabbitMQ Connection");

                    using (var connection = factory.CreateConnection())
                    using (var channel = connection.CreateModel())
                    {
                        channel.QueueDeclare(queue: "userInsertQueue", durable: false, exclusive: false, autoDelete: false, arguments: null);

                        string message = JsonConvert.SerializeObject(viewmodel);

                        var body = Encoding.UTF8.GetBytes(message);

                        IBasicProperties properties = channel.CreateBasicProperties();
                        properties.Persistent = true;
                        properties.DeliveryMode = 2;
                        properties.Headers = new Dictionary<string, object>();
                        properties.Headers.Add("senderUniqueId", senderUniqueId);

                        channel.BasicPublish(exchange: "",
                                             routingKey: "userInsertQueue",
                                             basicProperties: properties,
                                             body: body);
                    }

                    return View(viewmodel);
                }
                catch(Exception ex)
                {
                    return View(ex);
                }
            }

            return View(viewmodel);
        }
    }
}